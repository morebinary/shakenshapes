package
{
	import com.morebinary.ShakenShapes.ShakenShapesContext;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShakenShapes extends Sprite
	{
		protected var _context:ShakenShapesContext;
		
		public function ShakenShapes()
		{
			if(stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			_context = new ShakenShapesContext(this, true);
		}
	}
}