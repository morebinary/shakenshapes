package com.morebinary.ShakenShapes.services
{
	import org.robotlegs.core.IInjector;
	
	/**
	 * @author Paras Sheth
	 **/
	public class BootstrapServices
	{
		/**
		 * Constructor
		 */
		public function BootstrapServices(injector:IInjector)
		{
			injector.mapSingletonOf(IAssetsService, AssetsService);
		}
	}
}