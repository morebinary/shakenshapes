﻿package com.morebinary.ShakenShapes.services.parsers.fxgparser.parser 
{
	import com.morebinary.ShakenShapes.services.parsers.fxgparser.parser.model.Data;
	
	public interface IParser 
	{
		function parse(  data:Data ):void;
	}
	
}