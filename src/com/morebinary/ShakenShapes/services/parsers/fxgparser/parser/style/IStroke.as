package com.morebinary.ShakenShapes.services.parsers.fxgparser.parser.style
{
	public interface IStroke
	{
		function get weight():Number;
		function get miterLimit():Number;
		function get caps():String;
		function get joints():String;
	}
	
}