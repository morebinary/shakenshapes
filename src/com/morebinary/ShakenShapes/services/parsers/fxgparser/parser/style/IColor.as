package com.morebinary.ShakenShapes.services.parsers.fxgparser.parser.style
{
	
	public interface IColor 
	{
		function get color():uint;
		function get opacity():Number;
	}
	
}