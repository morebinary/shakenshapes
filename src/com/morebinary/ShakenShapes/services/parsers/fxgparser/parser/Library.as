﻿package com.morebinary.ShakenShapes.services.parsers.fxgparser.parser 
{
	import flash.display.DisplayObject;
	import com.morebinary.ShakenShapes.services.parsers.fxgparser.parser.IParser;
	import com.morebinary.ShakenShapes.services.parsers.fxgparser.parser.model.Data;
	import com.morebinary.ShakenShapes.services.parsers.fxgparser.parser.style.Style;
	import flash.display.Sprite;

	public class Library implements IParser
	{
		public static var LOCALNAME:String = "Library";
		
		public function Library() { }
		
		public function parse( data:Data ):void {}
	}

}