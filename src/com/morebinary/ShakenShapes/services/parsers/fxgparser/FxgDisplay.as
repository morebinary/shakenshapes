﻿package  com.morebinary.ShakenShapes.services.parsers.fxgparser
{
	import com.morebinary.ShakenShapes.services.parsers.fxgparser.parser.FxgFactory;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class FxgDisplay extends Sprite{
		
		private var _factory:FxgFactory;
		
		public function FxgDisplay(xml:XML = null) 
		{ 
			if(xml) parse(xml);
		}
		
		public function parse(xml:XML):void 
		{
			_factory = new FxgFactory();
			_factory.addEventListener( Event.COMPLETE, onComplete );
			_factory.parse(xml, this);
		}
		
		private function onComplete(event:Event):void
		{
			dispatchEvent( event );
		}
		
	}
	
}