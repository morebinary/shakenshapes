package com.morebinary.ShakenShapes.services
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.XMLLoader;
	import com.morebinary.ShakenShapes.controller.events.AssetsServiceEvent;
	import com.morebinary.ShakenShapes.model.IImageModel;
	import com.morebinary.ShakenShapes.model.vo.Image;
	
	import org.robotlegs.mvcs.Actor;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class AssetsService extends Actor implements IAssetsService
	{
		[Inject]
		public var imageModel:IImageModel;
		
		private var loadingQueue:LoaderMax;
		private var xml:XMLLoader;

		private var _imagesXMLList:XMLList;
		
		/**
		 * Constructor
		 */
		public function AssetsService()
		{
			LoaderMax.activate([XMLLoader, ImageLoader]);
		}
		
		/**
		 * Loads the data.xml file which contains urls to all the FXGs
		 */
		public function load():void
		{
			var xml:XMLLoader = new XMLLoader("assets/xml/data.xml", {name:"imageXML", onComplete:xmlLoadComplete, estimatedBytes:4000});
			
			xml.load();
		}
		
		/**
		 * Called on completion of the xml file
		 */
		private function xmlLoadComplete(event:LoaderEvent):void
		{
			xml = event.currentTarget as XMLLoader;
			loadingQueue = new LoaderMax({name:"mainQueue", onProgress:queueProgressHandler, onComplete:queueCompleteHandler, onError:queueErrorHandler});
			
			_imagesXMLList = xml.content.image;
			
			for each (var image:XML in _imagesXMLList)
			{
				loadingQueue.append(new XMLLoader(image, {name:image.@name}));
			}                      
			
			loadingQueue.load();
		}
		
		/**
		 * Handles the progress but there is no pre loader in this app so won't be used for now
		 */
		private function queueProgressHandler(event:LoaderEvent):void
		{
			// trace("Progress: " + event.target.progress);
		}
		
		/**
		 * Called if any queued item didn't load
		 */
		private function queueErrorHandler(event:LoaderEvent):void
		{
			trace("Error occured with " + event.target + ": " + event.text);
		}
		
		/**
		 * Called when the entire queue finished loading
		 */
		private function queueCompleteHandler(event:LoaderEvent):void
		{
			var imageCollection:Vector.<Image> = new Vector.<Image>;
			var i:uint = 0;
			for each (var image:XML in _imagesXMLList)
			{
				var imageVO:Image = new Image();
				imageVO.id = i;
				imageVO.imageData = event.target.content[i];
				imageVO.imageName = image.@name;
				imageCollection.push(imageVO);
				i++;
			}
			imageModel.imageCollection = imageCollection;
			
			var evt:AssetsServiceEvent = new AssetsServiceEvent(AssetsServiceEvent.LOAD_COMPLETED);
			dispatch(evt);
		}
	}
}