package com.morebinary.ShakenShapes.controller.bootstraps
{
	import com.morebinary.ShakenShapes.controller.commands.InitViewCommand;
	import com.morebinary.ShakenShapes.controller.commands.LoadAssetsCommand;
	import com.morebinary.ShakenShapes.controller.events.AssetsServiceEvent;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.core.ICommandMap;

	/**
	 * @author Paras Sheth
	 **/
	public class BootstrapCommands
	{
		/**
		 * Constructor
		 */
		public function BootstrapCommands(commandMap:ICommandMap)
		{
			commandMap.mapEvent(ContextEvent.STARTUP_COMPLETE, LoadAssetsCommand, ContextEvent, true);
			commandMap.mapEvent(AssetsServiceEvent.LOAD_COMPLETED, InitViewCommand, AssetsServiceEvent, true);
		}
	}
}