package com.morebinary.ShakenShapes.controller.bootstraps
{
	import com.morebinary.ShakenShapes.view.display.JoinShapeView;
	import com.morebinary.ShakenShapes.view.display.MainView;
	import com.morebinary.ShakenShapes.view.display.ShapeDisplayView;
	import com.morebinary.ShakenShapes.view.display.ShapeShakenView;
	import com.morebinary.ShakenShapes.view.mediators.JoinShapeMediator;
	import com.morebinary.ShakenShapes.view.mediators.MainViewMediator;
	import com.morebinary.ShakenShapes.view.mediators.ShapeDisplayMediator;
	import com.morebinary.ShakenShapes.view.mediators.ShapeShakenMediator;
	
	import org.robotlegs.core.IMediatorMap;
	
	/**
	 * @author Paras Sheth
	 **/
	public class BootstrapViewMediators extends Object
	{
		/**
		 * Constructor
		 * Bootstraps the mappings of the views
		 */
		public function BootstrapViewMediators(mediatorMap:IMediatorMap)
		{
			mediatorMap.mapView(MainView, MainViewMediator);
			mediatorMap.mapView(ShapeDisplayView, ShapeDisplayMediator);
			mediatorMap.mapView(ShapeShakenView, ShapeShakenMediator);
			mediatorMap.mapView(JoinShapeView, JoinShapeMediator);
		}
	}
}