package com.morebinary.ShakenShapes.controller.bootstraps
{
	import com.morebinary.ShakenShapes.model.IImageModel;
	import com.morebinary.ShakenShapes.model.ImagesModel;
	
	import org.robotlegs.core.IInjector;
	
	/**
	 * @author Paras Sheth
	 **/
	public class BootstrapModels
	{
		/**
		 * Constructor
		 */
		public function BootstrapModels(injector:IInjector)
		{
			injector.mapSingletonOf(IImageModel, ImagesModel);
		}
	}
}