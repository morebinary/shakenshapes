package com.morebinary.ShakenShapes.controller.commands
{
	import com.morebinary.ShakenShapes.services.IAssetsService;
	
	import org.robotlegs.mvcs.Command;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class LoadAssetsCommand extends Command
	{
		[Inject]
		public var assetsLoadingService:IAssetsService;
		
		override public function execute():void
		{
			assetsLoadingService.load();
			
			super.execute();
		}
	}
}