package com.morebinary.ShakenShapes.controller.commands
{
	import com.morebinary.ShakenShapes.view.display.MainView;
	
	import org.robotlegs.mvcs.Command;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class InitViewCommand extends Command
	{
		override public function execute():void
		{
			var mainView:MainView = new MainView();
			contextView.addChild(mainView);
			
			super.execute();
		}
	}
}