package com.morebinary.ShakenShapes.controller.events
{
	import flash.events.Event;
	
	public class ValidationEvent extends Event
	{
		public static const VALIDATE_ANSWER:String = "ValidationEvent.VALIDATE_ANSWER";
		
		public function get tileIndex():int{return _tileIndex;}
		private var _tileIndex:int;
		
		public function ValidationEvent(type:String, tileIndex:int)
		{
			_tileIndex = tileIndex;
			super(type);
		}
		
		override public function clone():Event
		{
			return new ValidationEvent(type, tileIndex);
		}
	}
}