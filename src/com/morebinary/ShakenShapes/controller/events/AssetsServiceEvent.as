package com.morebinary.ShakenShapes.controller.events
{
	import flash.events.Event;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class AssetsServiceEvent extends Event
	{
		public static const LOAD_FAILED:String = "AssetsServiceEvent.loadFailed";
		public static const LOAD_COMPLETED:String = "AssetsServiceEvent.loadCompleted";

		/**
		 * @constructor
		 */
		public function AssetsServiceEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new AssetsServiceEvent(type);
		}
		
	}
}