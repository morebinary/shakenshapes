package com.morebinary.ShakenShapes.controller.events
{
	import flash.events.Event;
	
	/**
	 * @author Paras Sheth
	 **/
	public class ResultEvent extends Event
	{
		public static const ANSWER_CORRECT:String = "ResultEvent.ANSWER_CORRECT";

		public function get tileIndex():int{return _tileIndex;}
		private var _tileIndex:int;
		
		public function ResultEvent(type:String, tileIndex:int)
		{
			_tileIndex = tileIndex;
			super(type);
		}
		
		override public function clone():Event
		{
			return new ResultEvent(type, tileIndex);
		}

	}
}