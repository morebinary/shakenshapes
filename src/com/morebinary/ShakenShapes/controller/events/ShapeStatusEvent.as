package com.morebinary.ShakenShapes.controller.events
{
	import flash.events.Event;
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShapeStatusEvent extends Event
	{
		public static const SHAKE_SOLVED:String = "ShapeStatusEvent.SHAKE_SOLVED";

		public function ShapeStatusEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new ShapeStatusEvent(type);
		}

	}
}