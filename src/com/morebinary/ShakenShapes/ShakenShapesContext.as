package com.morebinary.ShakenShapes
{
	import com.morebinary.ShakenShapes.controller.bootstraps.BootstrapCommands;
	import com.morebinary.ShakenShapes.controller.bootstraps.BootstrapModels;
	import com.morebinary.ShakenShapes.controller.bootstraps.BootstrapViewMediators;
	import com.morebinary.ShakenShapes.services.BootstrapServices;
	
	import flash.display.DisplayObjectContainer;
	
	import org.robotlegs.mvcs.Context;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShakenShapesContext extends Context
	{
		/**
		 * @Constructor
		 */
		public function ShakenShapesContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function startup():void
		{
			new BootstrapModels(injector);
			new BootstrapServices(injector);
			new BootstrapCommands(commandMap);
			new BootstrapViewMediators(mediatorMap);
			
			super.startup();
		}
	}
}