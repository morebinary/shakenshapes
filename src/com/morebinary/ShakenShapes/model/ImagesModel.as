package com.morebinary.ShakenShapes.model
{
	import com.morebinary.ShakenShapes.model.vo.Image;
	
	import org.robotlegs.mvcs.Actor;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class ImagesModel extends Actor implements IImageModel
	{
		public function get imageCollection():Vector.<Image>{return _imageCollection;}
		public function set imageCollection(images:Vector.<Image>):void
		{
			_imageCollection = images;
		}
		private var _imageCollection:Vector.<Image>;
		
		public function get currentIndex():uint{return _currentIndex;}
		public function set currentIndex(value:uint):void
		{
			_currentIndex = value;
		}
		private var _currentIndex:uint;

		public function get currentQuestionIndex():uint{return _currentQuestionIndex;}
		public function set currentQuestionIndex(value:uint):void
		{
			_currentQuestionIndex = value;
		}
		private var _currentQuestionIndex:uint;

	}
}