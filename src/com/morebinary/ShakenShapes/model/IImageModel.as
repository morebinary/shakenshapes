package com.morebinary.ShakenShapes.model
{
	import com.morebinary.ShakenShapes.model.vo.Image;
	
	/**
	 * @author Paras Sheth
	 **/
	public interface IImageModel
	{
		function get imageCollection():Vector.<Image>;
		
		function set imageCollection(images:Vector.<Image>):void;
		
		function get currentIndex():uint;

		function set currentIndex(value:uint):void;

		function get currentQuestionIndex():uint;

		function set currentQuestionIndex(value:uint):void;
	
	}
}