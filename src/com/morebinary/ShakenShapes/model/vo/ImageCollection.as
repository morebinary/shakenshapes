package com.morebinary.ShakenShapes.model.vo
{
	
	/**
	 * @author Paras Sheth
	 **/
	public class ImageCollection
	{
		public var images:Array = new Array();
	}
}