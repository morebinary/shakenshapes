package com.morebinary.ShakenShapes.model.vo
{
	
	/**
	 * @author Paras Sheth
	 **/
	public class Image
	{
		
		public function get id():uint{return _id;}
		public function set id(id:uint):void
		{
			_id = id;
		}
		private var _id:uint;
		
		public function get imageData():XML{return _imageData;}
		public function set imageData(imageData:XML):void
		{
			_imageData = imageData;
		}
		private var _imageData:XML;
		
		public function get imageName():String{return _imageName;}
		public function set imageName(imageName:String):void
		{
			_imageName = imageName;
		}
		private var _imageName:String;
		
	}
}