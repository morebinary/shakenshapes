package com.morebinary.ShakenShapes.view.mediators
{
	import com.morebinary.ShakenShapes.controller.events.ShapeStatusEvent;
	import com.morebinary.ShakenShapes.model.IImageModel;
	import com.morebinary.ShakenShapes.view.display.ShapeDisplayView;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShapeDisplayMediator extends Mediator
	{
		[Inject]
		public var view:ShapeDisplayView;
		[Inject]
		public var imagesModel:IImageModel;
		
		/**
		 * Handles registration of the view
		 */
		override public function onRegister():void
		{
			addContextListener(ShapeStatusEvent.SHAKE_SOLVED, changeShape, ShapeStatusEvent);
			view.displayShape(imagesModel.imageCollection[imagesModel.currentIndex].imageData);
			super.onRegister();
		}
		
		private function changeShape(event:ShapeStatusEvent):void
		{
			if(imagesModel.currentIndex < imagesModel.imageCollection.length)
			{
				view.removeShape();
				view.displayShape(imagesModel.imageCollection[imagesModel.currentIndex].imageData);
			}
		}
		
		/**
		 * Handles removal of the view
		 */
		override public function onRemove():void
		{
			removeContextListener(ShapeStatusEvent.SHAKE_SOLVED, changeShape, ShapeStatusEvent);
			super.onRemove();
		}
	}
}