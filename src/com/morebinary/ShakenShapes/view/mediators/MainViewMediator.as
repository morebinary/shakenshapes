package com.morebinary.ShakenShapes.view.mediators
{
	import com.morebinary.ShakenShapes.view.display.MainView;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class MainViewMediator extends Mediator
	{
		/**
		 * Handles registration of the view
		 */
		override public function onRegister():void
		{
			super.onRegister();
		}
		
		/**
		 * Handles removal of the view
		 */
		override public function onRemove():void
		{
			super.onRemove();
		}
	}
}