package com.morebinary.ShakenShapes.view.mediators
{
	import com.morebinary.ShakenShapes.controller.events.ResultEvent;
	import com.morebinary.ShakenShapes.controller.events.ShapeStatusEvent;
	import com.morebinary.ShakenShapes.controller.events.ValidationEvent;
	import com.morebinary.ShakenShapes.model.IImageModel;
	import com.morebinary.ShakenShapes.view.display.JoinShapeView;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class JoinShapeMediator extends Mediator
	{
		[Inject]
		public var view:JoinShapeView;
		[Inject]
		public var imagesModel:IImageModel;
		
		/**
		 * Handles registration of the view
		 */
		override public function onRegister():void
		{
			addContextListener(ValidationEvent.VALIDATE_ANSWER, validateAnswer, ValidationEvent);
			addContextListener(ShapeStatusEvent.SHAKE_SOLVED, showAllQuestions, ShapeStatusEvent);

			view.displayShape(imagesModel.imageCollection[imagesModel.currentIndex].imageData);
			view.showQuestion(imagesModel.currentQuestionIndex);
			super.onRegister();
		}
		
		/**
		 * Resets the view
		 */
		private function showAllQuestions(event:ShapeStatusEvent):void
		{
			if(imagesModel.currentIndex < imagesModel.imageCollection.length)
			{
				view.resetView();
				view.displayShape(imagesModel.imageCollection[imagesModel.currentIndex].imageData);
				view.showQuestion(imagesModel.currentQuestionIndex);
			}
		}
		
		/**
		 * Checks to see if the tile user clicked on was the right one or not
		 */
		private function validateAnswer(event:ValidationEvent):void
		{
			if(event.tileIndex == imagesModel.currentQuestionIndex)
			{
				var evt:ResultEvent = new ResultEvent(ResultEvent.ANSWER_CORRECT, int(event.tileIndex));
				dispatch(evt);
				view.hideQuestion(event.tileIndex);
				imagesModel.currentQuestionIndex ++;
				
				if(imagesModel.currentQuestionIndex < 9)
				{
					view.showQuestion(imagesModel.currentQuestionIndex);
				}
				else
				{
					if(imagesModel.currentIndex < imagesModel.imageCollection.length)
						imagesModel.currentIndex++;
					else
						imagesModel.currentIndex = 0;
						
					imagesModel.currentQuestionIndex = 0;
					
					var shapeSolvedEvent:ShapeStatusEvent = new ShapeStatusEvent(ShapeStatusEvent.SHAKE_SOLVED);
					dispatch(shapeSolvedEvent);
				}
			}
		}
		
		/**
		 * Handles removal of the view
		 */
		override public function onRemove():void
		{
			removeContextListener(ValidationEvent.VALIDATE_ANSWER, validateAnswer, ValidationEvent);
			removeContextListener(ShapeStatusEvent.SHAKE_SOLVED, showAllQuestions, ShapeStatusEvent);
			super.onRemove();
		}
	}
}