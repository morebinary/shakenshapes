package com.morebinary.ShakenShapes.view.mediators
{
	import com.morebinary.ShakenShapes.controller.events.ResultEvent;
	import com.morebinary.ShakenShapes.controller.events.ShapeStatusEvent;
	import com.morebinary.ShakenShapes.controller.events.ValidationEvent;
	import com.morebinary.ShakenShapes.model.IImageModel;
	import com.morebinary.ShakenShapes.view.display.ShapeShakenView;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShapeShakenMediator extends Mediator
	{
		[Inject]
		public var view:ShapeShakenView;
		[Inject]
		public var imagesModel:IImageModel;
		
		/**
		 * Handles registration of the view
		 */
		override public function onRegister():void
		{
			addViewListener(ValidationEvent.VALIDATE_ANSWER, dispatchToContextToValidate, ValidationEvent);
			addContextListener(ResultEvent.ANSWER_CORRECT, answerValidated, ResultEvent);
			addContextListener(ShapeStatusEvent.SHAKE_SOLVED, changeShakenShape, ShapeStatusEvent);
			view.displayShakenShape(imagesModel.imageCollection[imagesModel.currentIndex].imageData);
			super.onRegister();
		}
		
		/**
		 * Displays the next Shaken shape
		 */
		private function changeShakenShape(event:ShapeStatusEvent):void
		{
			if(imagesModel.currentIndex < imagesModel.imageCollection.length)
			{
				view.displayNextShakenShape(imagesModel.imageCollection[imagesModel.currentIndex].imageData);
			}
		}
		
		/**
		 * Answer was correct so the tile needs to be removed
		 */
		private function answerValidated(event:ResultEvent):void
		{
			view.removeTile(event.tileIndex);
		}
		
		/**
		 * dispatches the signal out to the signal
		 */
		private function dispatchToContextToValidate(event:ValidationEvent):void
		{
			dispatch(event);
		}
		
		/**
		 * Handles removal of the view
		 */
		override public function onRemove():void
		{
			removeViewListener(ValidationEvent.VALIDATE_ANSWER, dispatchToContextToValidate, ValidationEvent);
			removeContextListener(ResultEvent.ANSWER_CORRECT, answerValidated, ResultEvent);
			removeContextListener(ShapeStatusEvent.SHAKE_SOLVED, changeShakenShape, ShapeStatusEvent);
			super.onRemove();
		}
	}
}