package com.morebinary.ShakenShapes.view.display
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class MainView extends Sprite
	{
		private var _shapeDisplay:ShapeDisplayView;
		private var _shapeShapenDisplay:ShapeShakenView;
		private var _joinShapeDisplay:JoinShapeView;
		private var _container:Sprite;
		
		private var _stageWidth:Number;
		private var _stageHeight:Number;
		
		private const LINE_THICKNESS:Number = 3;
		private const GAP:Number = 50;
 		
		[Embed(source='assets/images/shakenshapes.png')]
		private var shakenShapesLogo:Class;
		private var _shakenShapesLogo:Bitmap;
		
		/**
		 * Constructor
		 */
		public function MainView()
		{
			if(stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * Shows the view
		 */
		public function init(event:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(Event.RESIZE, handleResize);
			
			_stageWidth = stage.stageWidth;
			_stageHeight = stage.stageHeight;
			
			_container = new Sprite();
			addChild(_container);
			
			addChild(_shakenShapesLogo = new shakenShapesLogo() as Bitmap);
			
			drawBorder();
			addShapeDisplayView();
			addShapeShakenView();
			addJoinShapeView();
			
			layoutChildren();
		}
		
		/**
		 * lays out the children
		 */
		private function layoutChildren():void
		{
			_shakenShapesLogo.x = _stageWidth - _shakenShapesLogo.width - LINE_THICKNESS;
			_shakenShapesLogo.y = LINE_THICKNESS;

			_container.x = _stageWidth*.5 - _container.width*.5;
			_container.y = _stageHeight*.5 - _container.height*.5;
		}
		
		/**
		 * Draws the border for the main view container
		 */
		private function drawBorder():void
		{
			this.graphics.clear();
			this.graphics.lineStyle(LINE_THICKNESS, 0x00, 1, true);
			this.graphics.drawRect(0, 0, _stageWidth - LINE_THICKNESS, _stageHeight - LINE_THICKNESS);
		}
		
		/**
		 * Adds Shape display view
		 */
		private function addShapeDisplayView():void
		{
			_shapeDisplay = new ShapeDisplayView();
			_container.addChild(_shapeDisplay);
			_shapeDisplay.x = 0;
		}
		
		/**
		 * Adds Shaken shape view
		 */
		private function addShapeShakenView():void
		{
			_shapeShapenDisplay = new ShapeShakenView();
			_container.addChild(_shapeShapenDisplay);
			_shapeShapenDisplay.x = _shapeDisplay.x + _shapeDisplay.width + GAP;
		}

		/**
		 * Adds Join shape view
		 */
		private function addJoinShapeView():void
		{
			_joinShapeDisplay = new JoinShapeView();
			_container.addChild(_joinShapeDisplay);
			_joinShapeDisplay.x = _shapeShapenDisplay.x + _shapeShapenDisplay.width + GAP;
		}
		
		/**
		 * Event Handler
		 * Handles stage resize
		 */
		protected function handleResize(event:Event):void
		{
			_stageWidth = stage.stageWidth;
			_stageHeight = stage.stageHeight;
			
			drawBorder();

			layoutChildren();
		}
		
	}
}