package com.morebinary.ShakenShapes.view.display
{
	import com.morebinary.ShakenShapes.view.factory.ShapeFactory;
	
	import flash.display.Sprite;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShapeDisplayView extends Sprite
	{
		private const LINE_THICKNESS:Number = 3;
		private const SHAPE_DISPLAY_WIDTH:Number = 350;
		private const SHAPE_DISPLAY_HEIGHT:Number = 250;
		
		private var _shapeFactory:ShapeFactory;
		
		/**
		 * Constructor
		 */
		public function ShapeDisplayView()
		{
			this.graphics.lineStyle(LINE_THICKNESS, 0x00, 1, true);
			this.graphics.drawRect(0, 0, SHAPE_DISPLAY_WIDTH + LINE_THICKNESS, SHAPE_DISPLAY_HEIGHT + LINE_THICKNESS);
			
			super();
		}
		
		/**
		 * Method that adds a shape to this view
		 */
		public function displayShape(imageData:XML):void
		{
			_shapeFactory = new ShapeFactory(imageData);
			addChild( _shapeFactory.createShape() );
		}
		
		/**
		 * removes the existing shape
		 */
		public function removeShape():void
		{
			while(this.numChildren)
			{
				this.removeChildAt(0);
			}
				
			_shapeFactory.destroyShape();
		}
	}
}