package com.morebinary.ShakenShapes.view.display
{
	import com.morebinary.ShakenShapes.controller.events.ValidationEvent;
	import com.morebinary.ShakenShapes.view.factory.ShakenShapeFactory;
	
	import flash.display.Sprite;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShapeShakenView extends Sprite
	{
		private var _shakenShapeFactory:ShakenShapeFactory;
		
		private const LINE_THICKNESS:Number = 3;
		private const SHAPE_DISPLAY_WIDTH:Number = 350;
		private const SHAPE_DISPLAY_HEIGHT:Number = 250;

		/**
		 * Constructor
		 */
		public function ShapeShakenView()
		{
			this.graphics.lineStyle(LINE_THICKNESS, 0x00, 1, true);
			this.graphics.drawRect(0, 0, SHAPE_DISPLAY_WIDTH + LINE_THICKNESS, SHAPE_DISPLAY_HEIGHT + LINE_THICKNESS);

			super();
		}
		
		/**
		 * Adds a shaken shape to this view
		 */
		public function displayShakenShape(imageData:XML):void
		{
			_shakenShapeFactory = new ShakenShapeFactory(imageData);
			_shakenShapeFactory.addEventListener(ValidationEvent.VALIDATE_ANSWER, handleTileClick);
			addChild( _shakenShapeFactory.createShakenShape() );
		}

		/**
		 * Shakes the next shape
		 */
		public function displayNextShakenShape(imageData:XML):void
		{
			if(_shakenShapeFactory.hasEventListener(ValidationEvent.VALIDATE_ANSWER))
				_shakenShapeFactory.removeEventListener(ValidationEvent.VALIDATE_ANSWER, handleTileClick);
				
			while(this.numChildren)
			{
				this.removeChildAt(0);
			}
				
			_shakenShapeFactory = new ShakenShapeFactory(imageData);
			_shakenShapeFactory.addEventListener(ValidationEvent.VALIDATE_ANSWER, handleTileClick);
			addChild( _shakenShapeFactory.createShakenShape() );
		}
		
		/**
		 * Handles Tile click
		 */
		protected function handleTileClick(event:ValidationEvent):void
		{
			dispatchEvent(new ValidationEvent(ValidationEvent.VALIDATE_ANSWER, event.tileIndex));
		}
		
		/**
		 * If user picked the correct tile it should get out of the way so it's easier for the user
		 */
		public function removeTile(tileIndex:int):void
		{
			_shakenShapeFactory.removeTile(tileIndex);
		}
	}
}