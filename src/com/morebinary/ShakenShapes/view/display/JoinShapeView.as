package com.morebinary.ShakenShapes.view.display
{
	import com.morebinary.ShakenShapes.view.factory.JoinShapeFactory;
	import com.morebinary.ShakenShapes.view.factory.ShapeFactory;
	
	import flash.display.Sprite;
	
	
	/**
	 * @author Paras Sheth
	 **/
	public class JoinShapeView extends Sprite
	{
		private const LINE_THICKNESS:Number = 3;
		private const SHAPE_DISPLAY_WIDTH:Number = 350;
		private const SHAPE_DISPLAY_HEIGHT:Number = 250;
		
		private var _shapeFactory:ShapeFactory;
		private var _gridFactory:JoinShapeFactory;
		
		/**
		 * Constructor
		 */
		public function JoinShapeView()
		{
			this.graphics.lineStyle(LINE_THICKNESS, 0x00, 1, true);
			this.graphics.drawRect(0, 0, SHAPE_DISPLAY_WIDTH + LINE_THICKNESS, SHAPE_DISPLAY_HEIGHT + LINE_THICKNESS);
			
			super();
		}
		
		/**
		 * Method that adds a shape to this view
		 */
		public function displayShape(imageData:XML):void
		{
			_shapeFactory = new ShapeFactory(imageData);
			addChild( _shapeFactory.createShape() );
			
			_gridFactory = new JoinShapeFactory();
			addChild( _gridFactory.createGrid() );
		}

		/**
		 * Resets the view
		 */
		public function resetView():void
		{
			_gridFactory.destroyGrid();
			
			while(this.numChildren)
				removeChildAt(0);
			
			_shapeFactory.destroyShape();
			_shapeFactory = null;
			_gridFactory = null;
		}

		/**
		 * shows a question
		 */
		public function showQuestion(number:int):void
		{
			_gridFactory.showQuestion(number);
		}

		/**
		 * Hides the question mark and shows the image
		 */
		public function hideQuestion(number:int):void
		{
			_gridFactory.hideQustion(number);
		}
	}
}