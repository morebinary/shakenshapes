package com.morebinary.ShakenShapes.view.factory
{
	import flash.display.Sprite;
	
	/**
	 * @author Paras Sheth
	 **/
	public interface IJoinShapeFactory
	{
		function createGrid():Sprite;
		function showQuestion(questionNumber:int):void;
	}
}