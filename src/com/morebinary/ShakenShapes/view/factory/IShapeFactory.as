package com.morebinary.ShakenShapes.view.factory
{
	import com.morebinary.ShakenShapes.services.parsers.fxgparser.FxgDisplay;
	
	/**
	 * @author Paras Sheth
	 **/
	public interface IShapeFactory
	{
		function createShape():FxgDisplay;
	}
}