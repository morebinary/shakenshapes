package com.morebinary.ShakenShapes.view.factory
{
	import flash.display.Sprite;
	
	/**
	 * @author Paras Sheth
	 **/
	public interface IShakenShapeFactory
	{
		function createShape():void;
		function createShakenShape():Sprite;
	}
}