package com.morebinary.ShakenShapes.view.factory
{
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * @author Paras Sheth
	 **/
	public class JoinShapeFactory implements IJoinShapeFactory
	{
		private var _thumbsGrid:Sprite;
		private var _questionsGrid:Sprite;
		private var _grid:Sprite;
		
		private var _baseBitmapData:BitmapData;
		private var _thumbBitmapData:BitmapData;
		private var _questionText:TextField;
		private var _questionFormat:TextFormat;

		private const GAP:Number = 0;
		
		/**
		 * Constructor
		 */
		public function JoinShapeFactory()
		{
			TweenPlugin.activate([GlowFilterPlugin]);
			
			_questionFormat = new TextFormat();
			_questionFormat.color = 0xffffff;
			_questionFormat.size = 30;
			_questionFormat.align =  TextFormatAlign.CENTER;
		}
		
		/**
		 * creates a grid 
		 */
		public function createGrid():Sprite
		{
			_grid = new Sprite();
			
			_thumbsGrid = new Sprite();
			_thumbsGrid.x = 25;
			_thumbsGrid.y = 25;
			
			_questionsGrid = new Sprite();
			_questionsGrid.x = 25;
			_questionsGrid.y = 25;
			
			_grid.addChild(_thumbsGrid);
			_grid.addChild(_questionsGrid);
			
			_baseBitmapData = new BitmapData(300, 200, true, 0xff000000);
			
			splitBitmap(_baseBitmapData, 3, 3);
			
			return _grid;
		}
		
		/**
		 * shows a question
		 */
		public function showQuestion(questionNumber:int):void
		{
			_questionsGrid.getChildAt(questionNumber).visible = true;
		}
		
		/**
		 * hides a question
		 */
		public function hideQustion(questionNumber:int):void
		{
			_thumbsGrid.getChildAt(questionNumber).visible = false;
			_questionsGrid.getChildAt(questionNumber).visible = false;
		}
		
		/**
		 * Generates unique random number from the given range
		 */
		private function findUniqueRandom(startNumber:int = 0, endNumber:int = 5):Array
		{
			var baseNumber:Array = new Array();
			var randomNumber:Array = new Array();
			
			for(var i:int = startNumber; i <= endNumber; i++)
			{
				baseNumber[i] = i;
			}
			
			for(i = endNumber; i > startNumber; i--)
			{
				var tmpRandom:Number = startNumber + Math.floor(Math.random() * (i - startNumber));
				randomNumber[i] = baseNumber[tmpRandom];
				baseNumber[tmpRandom] = baseNumber[i];
			}
			randomNumber[startNumber] = baseNumber[startNumber];
			return randomNumber;		
		}

		
		/**
		 * splits the provided bitmap data into rows and colums specified
		 */
		private function splitBitmap(bitmapSource:BitmapData, columns:int, rows:int):void
		{	
			var bitmapWidth:int = bitmapSource.width;
			var bitmapHeight:int = bitmapSource.height;		
			
			var thumbWidth:Number = Math.round(bitmapWidth / columns);
			var thumbHeight:Number = Math.round(bitmapHeight / rows);
			
			_thumbBitmapData = new BitmapData(bitmapWidth, bitmapHeight, true, 0xff000000);			
			_thumbBitmapData.draw(bitmapSource);
			
			var itemIndex:int = 0;
			
			for (var i:int = 0; i < columns; i++)
			{
				for (var j:int = 0; j < rows; j++) 
				{			
					var thumbBitmap:BitmapData = new BitmapData(bitmapWidth, bitmapHeight, true, 0xff000000);     
					thumbBitmap.copyPixels(_thumbBitmapData, new Rectangle(i * thumbWidth, j * thumbHeight, thumbWidth, thumbHeight), new Point(0, 0));
					
					var sprite:Sprite = new Sprite();
					sprite.graphics.beginBitmapFill(thumbBitmap, null, true);
					sprite.graphics.lineStyle(1, 0xffffff);
					sprite.graphics.drawRect(0, 0, thumbWidth, thumbHeight);
					sprite.name = String(itemIndex);
					sprite.x = i * (thumbWidth + GAP);
					sprite.y = j * (thumbHeight + GAP);
					
					_thumbsGrid.addChild(sprite);
					itemIndex++;
				}		
			}
			
			for(var l:int = 0; l < columns; l++)
			{
				for(var m:int = 0; m < rows; m++)
				{
					_questionText = new TextField();
					_questionText.autoSize =  TextFieldAutoSize.NONE;
					_questionText.defaultTextFormat = _questionFormat;
					_questionText.text = "?";
					_questionText.selectable = false;
					_questionText.x = l * (thumbWidth + GAP); 
					_questionText.y = (m * (thumbHeight + GAP) ) + _questionText.textHeight/2;
					_questionText.visible = false;
					_questionsGrid.addChild(_questionText);
				}
			}
		}
		
		/**
		 * clean up
		 */
		public function destroyGrid():void
		{
			_questionFormat = null;
			
			while(_thumbsGrid.numChildren)
				_thumbsGrid.removeChildAt(0);
			while(_questionsGrid.numChildren)
				_questionsGrid.removeChildAt(0);
			while(_grid.numChildren)
				_grid.removeChildAt(0);
			
			_thumbsGrid = null;
			_questionsGrid = null;
			_grid = null;
			
			_baseBitmapData.dispose();
		}
		
	}
}