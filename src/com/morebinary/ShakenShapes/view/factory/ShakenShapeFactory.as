package com.morebinary.ShakenShapes.view.factory
{
	import com.greensock.TweenMax;
	import com.greensock.plugins.AutoAlphaPlugin;
	import com.greensock.plugins.GlowFilterPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.morebinary.ShakenShapes.controller.events.ValidationEvent;
	import com.morebinary.ShakenShapes.services.parsers.fxgparser.FxgDisplay;
	
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShakenShapeFactory implements IShakenShapeFactory, IEventDispatcher
	{
		private var _shakenShape:Sprite;
		private var _fxgDisplay:FxgDisplay;
		private var _fxgXML:XML;
		
		private var _baseBitmapData:BitmapData;
		private var _thumbBitmapData:BitmapData;
		private var _dispatcher:IEventDispatcher;

		private const GAP:Number = 10;
		
		/**
		 * Constructor
		 */
		public function ShakenShapeFactory(imageData:XML)
		{
			TweenPlugin.activate([AutoAlphaPlugin, GlowFilterPlugin]);
			_fxgXML = imageData;
			_dispatcher = new EventDispatcher();
		}
		
		/**
		 * Parses fxg data and returns a Sprite
		 */
		public function createShape():void
		{
			_fxgDisplay = new FxgDisplay(_fxgXML);
			_fxgDisplay.width = 300;
			_fxgDisplay.height = 200;
			_fxgDisplay.cacheAsBitmap = true;
		}
		
		/**
		 * Splits the image in three rows and columns 
		 * and shuffles the pieces around
		 */
		public function createShakenShape():Sprite
		{
			_fxgDisplay = new FxgDisplay(_fxgXML);
			_fxgDisplay.width = 300;
			_fxgDisplay.height = 200;
			_fxgDisplay.cacheAsBitmap = true;
			
			// temporary sprite for drawing
			var sprite:Sprite = new Sprite();
			sprite.addChild(_fxgDisplay);
			
			_shakenShape = new Sprite();
			_shakenShape.x = 16.5;
			_shakenShape.y = 16.5;
			
			_baseBitmapData = new BitmapData(_fxgDisplay.width, _fxgDisplay.height, true, 0x00000000);
			_baseBitmapData.draw(sprite);
			
			splitBitmap(_baseBitmapData, 3, 3);
			
			shuffleBitmaps();
			
			// clean up
			sprite.removeChild(_fxgDisplay);
			sprite = null;
			
			return _shakenShape;
		}
		
		/**
		 * places broken bitmaps randomly in the grid
		 */
		private function shuffleBitmaps():void
		{
			var randomNumbers:Array = findUniqueRandom(0, _shakenShape.numChildren -1);
			
			for (var j:int = 0; j < randomNumbers.length; j++)
			{
				var randomNum:Number = Math.round(Math.random() * (randomNumbers.length-1));
				var firstX:Number = _shakenShape.getChildAt(randomNumbers[j]).x;
				var firstY:Number = _shakenShape.getChildAt(randomNumbers[j]).y;
				var secondX:Number = _shakenShape.getChildAt(randomNum).x;
				var secondY:Number = _shakenShape.getChildAt(randomNum).y;
				
				_shakenShape.getChildAt(randomNumbers[j]).x = secondX;
				_shakenShape.getChildAt(randomNumbers[j]).y = secondY;
				
				_shakenShape.getChildAt(randomNum).x = firstX;
				_shakenShape.getChildAt(randomNum).y = firstY;
			}
		}
		
		/**
		 * Generates unique random number from the given range
		 */
		private function findUniqueRandom(startNumber:int = 0, endNumber:int = 5):Array
		{
			var baseNumber:Array = new Array();
			var randomNumber:Array = new Array();
			
			for(var i:int = startNumber; i <= endNumber; i++)
			{
				baseNumber[i] = i;
			}
			
			for(i = endNumber; i > startNumber; i--)
			{
				var tmpRandom:Number = startNumber + Math.floor(Math.random() * (i - startNumber));
				randomNumber[i] = baseNumber[tmpRandom];
				baseNumber[tmpRandom] = baseNumber[i];
			}
			randomNumber[startNumber] = baseNumber[startNumber];
			return randomNumber;		
		}

		
		/**
		 * splits the provided bitmap data into rows and colums specified
		 */
		private function splitBitmap(bitmapSource:BitmapData, columns:int, rows:int):void
		{	
			var bitmapWidth:int = bitmapSource.width;
			var bitmapHeight:int = bitmapSource.height;		
			
			var thumbWidth:Number = Math.round(bitmapWidth / columns);
			var thumbHeight:Number = Math.round(bitmapHeight / rows);
			
			_thumbBitmapData = new BitmapData(bitmapWidth, bitmapHeight, true, 0x00000000);			
			_thumbBitmapData.draw(bitmapSource);
			
			var itemIndex:int = 0;
			
			for (var i:int = 0; i < columns; i++) { 			
				for (var j:int = 0; j < rows; j++) 
				{			
					var thumbBitmap:BitmapData = new BitmapData(bitmapWidth, bitmapHeight, true, 0x00000000);     
					thumbBitmap.copyPixels(_thumbBitmapData, new Rectangle(i * thumbWidth, j * thumbHeight, thumbWidth, thumbHeight), new Point(0, 0));
					
					var sprite:Sprite = new Sprite();
					sprite.graphics.beginBitmapFill(thumbBitmap, null, true);
					sprite.graphics.lineStyle(1);
					sprite.graphics.drawRect(0, 0, thumbWidth, thumbHeight);
					sprite.buttonMode = true;
					sprite.name = String(itemIndex);
					sprite.addEventListener(MouseEvent.CLICK, handleSpriteClick);
					sprite.addEventListener(MouseEvent.ROLL_OVER, handleSpriteOver);
					sprite.addEventListener(MouseEvent.ROLL_OUT, handleSpriteOut);
					sprite.x = i * (thumbWidth + GAP); 
					sprite.y = j * (thumbHeight + GAP);
					_shakenShape.addChild(sprite);
					itemIndex++;
				}		
			}		
		}
		
		/**
		 * Shows the glow effect on mouse roll over
		 */
		protected function handleSpriteClick(event:MouseEvent):void
		{
			var userResponseEvent:ValidationEvent = new ValidationEvent(ValidationEvent.VALIDATE_ANSWER, int(event.target.name));
			dispatchEvent(userResponseEvent);
		}

		/**
		 * Shows the glow effect on mouse roll over
		 */
		private function handleSpriteOver(event:MouseEvent):void
		{
			TweenMax.to(event.currentTarget, .5, {glowFilter:{color:0xff0000, alpha:1, strength:1, quality:3, blurX:5, blurY:5}});
		}

		/**
		 * Hides the glow effect on mouse roll out
		 */
		private function handleSpriteOut(event:MouseEvent):void
		{
			TweenMax.to(event.currentTarget, .5, {glowFilter:{alpha:0, remove:true}});
		}
		
		/**
		 * Removes/hides the tile
		 */
		public function removeTile(tileIndex:int):void
		{
			TweenMax.to(_shakenShape.getChildAt(tileIndex), 0.7, {autoAlpha:0 });
		}
		
		public function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false):void
		{
			_dispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		public function dispatchEvent(event:Event):Boolean
		{
			return _dispatcher.dispatchEvent(event);
		}
		
		public function hasEventListener(type:String):Boolean
		{
			return _dispatcher.hasEventListener(type);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void
		{
			_dispatcher.removeEventListener(type, listener, useCapture);
		}

		public function willTrigger(type:String):Boolean
		{
			return _dispatcher.willTrigger(type);
		}
	}
}