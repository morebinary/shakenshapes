package com.morebinary.ShakenShapes.view.factory
{
	import com.morebinary.ShakenShapes.services.parsers.fxgparser.FxgDisplay;
	
	/**
	 * @author Paras Sheth
	 **/
	public class ShapeFactory implements IShapeFactory
	{
		private var _fxgDisplay:FxgDisplay;
		private var _fxgXML:XML;
		
		/**
		 * Constructor
		 */
		public function ShapeFactory(imageData:XML)
		{
			_fxgXML = imageData;
		}
		
		/**
		 * Parses fxg data and returns a Sprite
		 */
		public function createShape():FxgDisplay
		{
			_fxgDisplay = new FxgDisplay(_fxgXML);
			_fxgDisplay.x = 25;
			_fxgDisplay.y = 25;
			_fxgDisplay.width = 300;
			_fxgDisplay.height = 200;
			_fxgDisplay.cacheAsBitmap = true;
			return _fxgDisplay;	
		}

		/**
		 * cleans up
		 */
		public function destroyShape():void
		{
			_fxgDisplay = null;
		}
	}
}